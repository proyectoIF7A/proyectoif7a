﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProyectoWebServer.Models;

namespace ProyectoWebServer.Controllers
{
    public class AvesController : ApiController
    {
        Ave[] ani2 = new Ave[] {
        new Ave{idave=2001,nombreave="Periquito",colorave="Verde",razaave="Australeanos",estaturaave="15 cm",pesoave="35 g"},
       new Ave{idave=2002,nombreave="Gallina",colorave="Blanca",razaave="Asil",estaturaave="40 cm",pesoave="500 g"},
       new Ave{idave=2003,nombreave="Cuervo",colorave="Negro",razaave="Corvidae",estaturaave="52 cm",pesoave="1 kg"},
       new Ave{idave=2004,nombreave="Ganzo",colorave="Blanco",razaave="Anser",estaturaave="90 cm",pesoave="12 kg"},
       new Ave{idave=2005,nombreave="Paloma",colorave="Gris",razaave="Columbidae",estaturaave="30 cm",pesoave="400 g"},
       };
              public IEnumerable<Ave> GetAllaves()
        {
            return ani2;
        }
        public IHttpActionResult Getaves(int id)
        {
            var avess = ani2.FirstOrDefault((c) => c.idave == id);
            if (avess != null)
            {
                return Ok(avess);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
