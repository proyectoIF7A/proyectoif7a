﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProyectoWebServer.Models;

namespace ProyectoWebServer.Controllers
{
    public class ReptilesController : ApiController
    {
        Reptil[] ani3 = new Reptil[] {
        new Reptil{idreptil=3001,nombrereptil="Tortuga",colorreptil="Cafe",razareptil="Marina",estaturareptil="70 cm",pesoreptil="300 kg"},
       new Reptil{idreptil=3002,nombrereptil="Serpiente",colorreptil="Pinta",razareptil="Vibora",estaturareptil="60 cm",pesoreptil="4 kg"},
       new Reptil{idreptil=3003,nombrereptil="Lagartija",colorreptil="Verde",razareptil="Iguana",estaturareptil="14 cm",pesoreptil="15 kg"},
       new Reptil{idreptil=3004,nombrereptil="Cocodrilo",colorreptil="Cafe",razareptil="Caiman",estaturareptil="100 cm",pesoreptil="5 kg"},
       new Reptil{idreptil=3005,nombrereptil="Lagarto",colorreptil="Negro",razareptil="Sauropsida",estaturareptil="60 cm",pesoreptil="300 g"}
        };
        public IEnumerable<Reptil> GetAllreptiles()
        {
            return ani3;
        }
        public IHttpActionResult Getreptiles(int id)
        {
            var reptiless = ani3.FirstOrDefault((c) => c.idreptil == id);
            if (reptiless != null)
            {
                return Ok(reptiless);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
