﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProyectoWebServer.Models;

namespace ProyectoWebServer.Controllers
{
    public class FelinosController : ApiController
    {

        Felino[] ani = new Felino[] {
            //Feninos
       new Felino{idfelino=1001,nombrefelino="Tigre",colorfelino="Blanco",razafelino="Bengala",estaturafelino="120 cm",pesofelino="285 kg"},
       new Felino{idfelino=1002,nombrefelino="Gato",colorfelino="Gris",razafelino="Pescador",estaturafelino="25 cm",pesofelino="4 kg"},
       new Felino{idfelino=1003,nombrefelino="Lince",colorfelino="Dorado",razafelino="Iberico",estaturafelino="80 cm",pesofelino="30 kg"},
       new Felino{idfelino=1004,nombrefelino="Leopardo",colorfelino="Pinto",razafelino="De las nieves",estaturafelino="80 cm",pesofelino="70 kg"},
       new Felino{idfelino=1005,nombrefelino="Guepardo",colorfelino="Pinto",razafelino="Asiatico",estaturafelino="90 cm",pesofelino="65 kg"},
  
        };


        public IEnumerable<Felino> GetAllfelinos()
        {
            return ani;

        }
        public IHttpActionResult Getfelinos(int id)
        {
            var felinoss = ani.FirstOrDefault((c) => c.idfelino == id);
            if (felinoss != null)
            {
                return Ok(felinoss);
            }
            else
            {
                return NotFound();
            }
        }


    }
}
