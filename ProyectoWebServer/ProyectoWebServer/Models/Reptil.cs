﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoWebServer.Models
{
    public class Reptil
    {
        public int idreptil { get; set; }
        public string nombrereptil { get; set; }
        public string colorreptil { get; set; }
        public string razareptil { get; set; }
        public string estaturareptil { get; set; }
        public string pesoreptil { get; set; }
    }
}