﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoWebServer.Models
{
    public class Felino
    {
        //Felinos
        public int idfelino { get; set; }
        public string nombrefelino { get; set; }
        public string colorfelino { get; set; }
        public string razafelino { get; set; }
        public string estaturafelino { get; set; }
        public string pesofelino { get; set; }
        //public string tipodeanimal { get; set; }
    }
 }