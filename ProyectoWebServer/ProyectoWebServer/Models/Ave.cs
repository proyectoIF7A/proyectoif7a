﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoWebServer.Models
{
    public class Ave
    {
        public int idave { get; set; }
        public string nombreave { get; set; }
        public string colorave { get; set; }
        public string razaave { get; set; }
        public string estaturaave { get; set; }
        public string pesoave { get; set; }
    }
}