CREATE DATABASE MUSICAB
USE MUSICAB

CREATE TABLE Categoria(
NoCategoria int primary key not null,
NombredeCategoria varchar(50)  
)

CREATE TABLE Artistas(
NoArtista int primary key not null,
NombredeArtista varchar(50),
FKTipodeCategoria int foreign key references Categoria(NoCategoria)
)

CREATE TABLE Canciones (
NoCancion int primary key not null,
NombredeCancion varchar(50),
AniodeEstreno varchar(4),
FKNombredeArtista int foreign key references Artistas(NoArtista)
)

CREATE TABLE Albumes(
NoAlbum int primary key not null,
NombredelAlbum varchar(50),
ContenidodeCanciones int,
AniodeEstreno varchar(4),
FKNombredeArtista int foreign key references Artistas(NoArtista)
)
 
CREATE TABLE PistasGuardadas (
Titulo nvarchar(50),
Artista nvarchar(50),
Album nvarchar(50));

CREATE TABLE ListadeReproduccion (
Titulo nvarchar(50));

/*300-399 Categoria*/
Insert into Categoria values ('300','Pop')
Insert into Categoria values ('301','Rock')
Insert into Categoria values ('302','Reggaeton')
Insert into Categoria values ('303','Clasica')
Insert into Categoria values ('304','Reggae')
Insert into Categoria values ('305','Rap')
Insert into Categoria values ('306','Romantica')
Insert into Categoria values ('307','Metalica')
Insert into Categoria values ('308','Cumbia')
Insert into Categoria values ('309','Grupera')
Insert into Categoria values ('310','Romantica')

/*200-299 Artistas */
Insert into Artistas values ('200','Austin Mahone','300')
Insert into Artistas values ('201','Hardwell','310')
Insert into Artistas values ('202','Becky G','300')
Insert into Artistas values ('203','Camila Cabello','308')
Insert into Artistas values ('204','Cody Simpsom','300')
Insert into Artistas values ('205','Enrique Iglesias','300')
Insert into Artistas values ('206','Calimba','306')
Insert into Artistas values ('207','Codeko','305')
Insert into Artistas values ('208','Florida','307')
Insert into Artistas values ('209','R. Kelly','300')
Insert into Artistas values ('210','Greyson Chanse','300')


/*100-199 Canciones*/
Insert into Canciones values ('100', 'Send It','2017','200')
Insert into Canciones values ('101', 'All Ever Need','2016','200')
Insert into Canciones values ('102', 'Except for Us','2017','200')
Insert into Canciones values ('103', 'Put lt on Me','2016','201')
Insert into Canciones values ('104', 'Lady','2016','203')
Insert into Canciones values ('105', 'Creatures Of The Night','2017','205')
Insert into Canciones values ('106', 'Pretty and Young','2017','207')
Insert into Canciones values ('107', 'Found You','2017','200')
Insert into Canciones values ('108', 'Mmm Yeah','2015','200')
Insert into Canciones values ('109', 'Way Up','2017','200')
Insert into Canciones values ('110', 'Say Hi','2017','200')


/*400-499 Albumes*/
Insert into Albumes values ('400','The Secret', '8','2016','200')
Insert into Albumes values ('401','This ls', '8','2016','200')
Insert into Albumes values ('402','ForMe You', '8','2016','201')
Insert into Albumes values ('403','This Ls Not The Album', '8','2016','203')
Insert into Albumes values ('404','Sil', '10','2016','205')

/*Consultas*/
 Select*from Canciones
 Select*from Artistas
 Select*from Categoria
 Select*from Albumes
