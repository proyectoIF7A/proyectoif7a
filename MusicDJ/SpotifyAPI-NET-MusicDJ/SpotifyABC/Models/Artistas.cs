//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpotifyABC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Artistas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Artistas()
        {
            this.Albumes = new HashSet<Albumes>();
            this.Canciones = new HashSet<Canciones>();
        }
    
        public int NoArtista { get; set; }
        public string NombredeArtista { get; set; }
        public Nullable<int> Tipo { get; set; }
        public Nullable<int> FKTipodeCategoria { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Albumes> Albumes { get; set; }
        public virtual Categoria Categoria { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Canciones> Canciones { get; set; }
    }
}
