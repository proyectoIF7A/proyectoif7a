﻿using System.Windows.Forms;
using SpotifyAPI.Example;
using SpotifyAPI.Web.Models;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace SpotifyAPI.Example
{
    public partial class ExampleForm : Form
    {
        public ExampleForm()
        {
            InitializeComponent();
        }

        private void webControl2_Load(object sender, System.EventArgs e)
        {

        }

        private void ExampleForm_Load(object sender, System.EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'mUSICABDataSet.Albumes' Puede moverla o quitarla según sea necesario.
            this.albumesTableAdapter.Fill(this.mUSICABDataSet.Albumes);
            // TODO: esta línea de código carga datos en la tabla 'mUSICABDataSet.Canciones' Puede moverla o quitarla según sea necesario.
            this.cancionesTableAdapter.Fill(this.mUSICABDataSet.Canciones);
            // TODO: esta línea de código carga datos en la tabla 'mUSICABDataSet.Artistas' Puede moverla o quitarla según sea necesario.
            this.artistasTableAdapter.Fill(this.mUSICABDataSet.Artistas);
            // TODO: esta línea de código carga datos en la tabla 'mUSICABDataSet.Categoria' Puede moverla o quitarla según sea necesario.
            this.categoriaTableAdapter.Fill(this.mUSICABDataSet.Categoria);


        }

        private void btnGuardarCategoria_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (MessageBox.Show("Estas seguro que quieres Guardar todos los cambios", "Mensaje - Categoria", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    categoriaBindingSource.EndEdit();
                    categoriaTableAdapter.Update(this.mUSICABDataSet.Categoria);
                    MessageBox.Show("Cambios Guardados Correctamente", "Listo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operacion cancelada", "Mensaje - Categoria", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Posible error: " + ex, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor = Cursors.Default;
        }

        private void btnGuardarArtistas_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (MessageBox.Show("Estas seguro que quieres Guardar todos los cambios", "Mensaje - Artistas", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    artistasBindingSource.EndEdit();
                    artistasTableAdapter.Update(this.mUSICABDataSet.Artistas);
                    MessageBox.Show("Cambios Guardados Correctamente", "Listo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operacion cancelada", "Mensaje - Artistas", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Posible error: " + ex, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor = Cursors.Default;

        }

        private void btnGuardarCanciones_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (MessageBox.Show("Estas seguro que quieres Guardar todos los cambios", "Mensaje - Canciones", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    cancionesBindingSource.EndEdit();
                    cancionesTableAdapter.Update(this.mUSICABDataSet.Canciones);
                    MessageBox.Show("Cambios Guardados Correctamente", "Listo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operacion cancelada", "Mensaje - Canciones", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Posible error: " + ex, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor = Cursors.Default;

        }

        private void btnGuardarAlbumes_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (MessageBox.Show("Estas seguro que quieres Guardar todos los cambios", "Mensaje - Albumes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    albumesBindingSource.EndEdit();
                    albumesTableAdapter.Update(this.mUSICABDataSet.Albumes);
                    MessageBox.Show("Cambios Guardados Correctamente", "Listo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operacion cancelada", "Mensaje - Albumes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Posible error: " + ex, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor = Cursors.Default;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
