﻿namespace SpotifyAPI.Example
{
    partial class WebControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WebControl));
            this.authButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.savedTracksListView22 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.savedTracksCountLabel = new System.Windows.Forms.Label();
            this.playlistsListBox = new System.Windows.Forms.ListBox();
            this.playlistsCountLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.displayNameLabel = new System.Windows.Forms.Label();
            this.countryLabel = new System.Windows.Forms.Label();
            this.accountLabel = new System.Windows.Forms.Label();
            this.avatarPictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pistasGuardadasBindingSource = new System.Windows.Forms.BindingSource(this.components);

            this.artistasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mUSICABDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.albumesBindingSource = new System.Windows.Forms.BindingSource(this.components);
     
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pistasGuardadasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.artistasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mUSICABDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // authButton
            // 
            this.authButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.authButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.authButton.Location = new System.Drawing.Point(6, 3);
            this.authButton.Name = "authButton";
            this.authButton.Size = new System.Drawing.Size(951, 48);
            this.authButton.TabIndex = 0;
            this.authButton.Text = "Autentificate con SpotifyWeb API";
            this.authButton.UseVisualStyleBackColor = false;
            this.authButton.Click += new System.EventHandler(this.authButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(1, 395);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Usuario:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(1, 412);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 19);
            this.label4.TabIndex = 6;
            this.label4.Text = "País:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(1, 431);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 19);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tipo de cuenta:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(252, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 19);
            this.label2.TabIndex = 9;
            this.label2.Text = "Pistas guardadas:";
            // 
            // savedTracksListView22
            // 
            this.savedTracksListView22.BackColor = System.Drawing.SystemColors.MenuText;
            this.savedTracksListView22.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.savedTracksListView22.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.savedTracksListView22.ForeColor = System.Drawing.SystemColors.Window;
            this.savedTracksListView22.FullRowSelect = true;
            this.savedTracksListView22.Location = new System.Drawing.Point(250, 144);
            this.savedTracksListView22.Name = "savedTracksListView22";
            this.savedTracksListView22.Size = new System.Drawing.Size(385, 381);
            this.savedTracksListView22.TabIndex = 10;
            this.savedTracksListView22.UseCompatibleStateImageBehavior = false;
            this.savedTracksListView22.View = System.Windows.Forms.View.Details;
            this.savedTracksListView22.SelectedIndexChanged += new System.EventHandler(this.savedTracksListView_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Titulo";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Artista";
            this.columnHeader2.Width = 117;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Album";
            this.columnHeader3.Width = 131;
            // 
            // savedTracksCountLabel
            // 
            this.savedTracksCountLabel.AutoSize = true;
            this.savedTracksCountLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedTracksCountLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.savedTracksCountLabel.Location = new System.Drawing.Point(402, 122);
            this.savedTracksCountLabel.Name = "savedTracksCountLabel";
            this.savedTracksCountLabel.Size = new System.Drawing.Size(16, 19);
            this.savedTracksCountLabel.TabIndex = 11;
            this.savedTracksCountLabel.Text = "-";
            // 
            // playlistsListBox
            // 
            this.playlistsListBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.playlistsListBox.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.playlistsListBox.ForeColor = System.Drawing.SystemColors.Window;
            this.playlistsListBox.FormattingEnabled = true;
            this.playlistsListBox.ItemHeight = 21;
            this.playlistsListBox.Location = new System.Drawing.Point(652, 144);
            this.playlistsListBox.Name = "playlistsListBox";
            this.playlistsListBox.Size = new System.Drawing.Size(305, 382);
            this.playlistsListBox.TabIndex = 12;
            this.playlistsListBox.SelectedIndexChanged += new System.EventHandler(this.playlistsListBox_SelectedIndexChanged);
            // 
            // playlistsCountLabel
            // 
            this.playlistsCountLabel.AutoSize = true;
            this.playlistsCountLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playlistsCountLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.playlistsCountLabel.Location = new System.Drawing.Point(840, 122);
            this.playlistsCountLabel.Name = "playlistsCountLabel";
            this.playlistsCountLabel.Size = new System.Drawing.Size(16, 19);
            this.playlistsCountLabel.TabIndex = 14;
            this.playlistsCountLabel.Text = "-";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(648, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(186, 19);
            this.label7.TabIndex = 13;
            this.label7.Text = "Listas de reproducción:";
            // 
            // displayNameLabel
            // 
            this.displayNameLabel.AutoSize = true;
            this.displayNameLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayNameLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.displayNameLabel.Location = new System.Drawing.Point(68, 395);
            this.displayNameLabel.Name = "displayNameLabel";
            this.displayNameLabel.Size = new System.Drawing.Size(16, 19);
            this.displayNameLabel.TabIndex = 15;
            this.displayNameLabel.Text = "-";
            // 
            // countryLabel
            // 
            this.countryLabel.AutoSize = true;
            this.countryLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countryLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.countryLabel.Location = new System.Drawing.Point(39, 412);
            this.countryLabel.Name = "countryLabel";
            this.countryLabel.Size = new System.Drawing.Size(16, 19);
            this.countryLabel.TabIndex = 16;
            this.countryLabel.Text = "-";
            // 
            // accountLabel
            // 
            this.accountLabel.AutoSize = true;
            this.accountLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.accountLabel.Location = new System.Drawing.Point(130, 431);
            this.accountLabel.Name = "accountLabel";
            this.accountLabel.Size = new System.Drawing.Size(16, 19);
            this.accountLabel.TabIndex = 18;
            this.accountLabel.Text = "-";
            // 
            // avatarPictureBox
            // 
            this.avatarPictureBox.Location = new System.Drawing.Point(14, 198);
            this.avatarPictureBox.Name = "avatarPictureBox";
            this.avatarPictureBox.Size = new System.Drawing.Size(201, 178);
            this.avatarPictureBox.TabIndex = 19;
            this.avatarPictureBox.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(14, 59);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(228, 131);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // 
            // WebControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.avatarPictureBox);
            this.Controls.Add(this.accountLabel);
            this.Controls.Add(this.countryLabel);
            this.Controls.Add(this.displayNameLabel);
            this.Controls.Add(this.playlistsCountLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.playlistsListBox);
            this.Controls.Add(this.savedTracksCountLabel);
            this.Controls.Add(this.savedTracksListView22);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.authButton);
            this.Name = "WebControl";
            this.Size = new System.Drawing.Size(979, 552);
            this.Load += new System.EventHandler(this.WebControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pistasGuardadasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.artistasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mUSICABDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button authButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView savedTracksListView22;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label savedTracksCountLabel;
        private System.Windows.Forms.ListBox playlistsListBox;
        private System.Windows.Forms.Label playlistsCountLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label displayNameLabel;
        private System.Windows.Forms.Label countryLabel;
        private System.Windows.Forms.Label accountLabel;
        private System.Windows.Forms.PictureBox avatarPictureBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.BindingSource artistasBindingSource;
        private System.Windows.Forms.BindingSource mUSICABDataSetBindingSource;
        private System.Windows.Forms.BindingSource albumesBindingSource;
        private System.Windows.Forms.BindingSource pistasGuardadasBindingSource;
    }
}
