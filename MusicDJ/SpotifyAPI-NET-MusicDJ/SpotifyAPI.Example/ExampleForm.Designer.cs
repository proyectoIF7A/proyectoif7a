﻿namespace SpotifyAPI.Example
{
    partial class ExampleForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExampleForm));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblIntrucciones = new System.Windows.Forms.Label();
            this.lblAlbumes = new System.Windows.Forms.Label();
            this.lblCanciones = new System.Windows.Forms.Label();
            this.btnGuardarAlbumes = new System.Windows.Forms.Button();
            this.btnGuardarCanciones = new System.Windows.Forms.Button();
            this.btnGuardarArtistas = new System.Windows.Forms.Button();
            this.btnGuardarCategoria = new System.Windows.Forms.Button();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.dgvCategoria = new System.Windows.Forms.DataGridView();
            this.noCategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombredeCategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoriaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mUSICABDataSet = new SpotifyAPI.Example.MUSICABDataSet();
            this.lblArtistas = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dgvCanciones = new System.Windows.Forms.DataGridView();
            this.noCancionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombredeCancionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aniodeEstrenoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.artistasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cancionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvAlbumes = new System.Windows.Forms.DataGridView();
            this.noAlbumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombredelAlbumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contenidodeCancionesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aniodeEstrenoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.albumesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvArtistas = new System.Windows.Forms.DataGridView();
            this.noArtistaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombredeArtistaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fKTipodeCategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.webControl2 = new SpotifyAPI.Example.WebControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.localControl1 = new SpotifyAPI.Example.LocalControl();
            this.ABC = new System.Windows.Forms.TabControl();
            this.categoriaTableAdapter = new SpotifyAPI.Example.MUSICABDataSetTableAdapters.CategoriaTableAdapter();
            this.artistasTableAdapter = new SpotifyAPI.Example.MUSICABDataSetTableAdapters.ArtistasTableAdapter();
            this.cancionesTableAdapter = new SpotifyAPI.Example.MUSICABDataSetTableAdapters.CancionesTableAdapter();
            this.albumesTableAdapter = new SpotifyAPI.Example.MUSICABDataSetTableAdapters.AlbumesTableAdapter();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mUSICABDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCanciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.artistasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancionesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlbumes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArtistas)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.ABC.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Highlight;
            this.tabPage3.Controls.Add(this.listBox1);
            this.tabPage3.Controls.Add(this.lblIntrucciones);
            this.tabPage3.Controls.Add(this.lblAlbumes);
            this.tabPage3.Controls.Add(this.lblCanciones);
            this.tabPage3.Controls.Add(this.btnGuardarAlbumes);
            this.tabPage3.Controls.Add(this.btnGuardarCanciones);
            this.tabPage3.Controls.Add(this.btnGuardarArtistas);
            this.tabPage3.Controls.Add(this.btnGuardarCategoria);
            this.tabPage3.Controls.Add(this.lblCategoria);
            this.tabPage3.Controls.Add(this.dgvCategoria);
            this.tabPage3.Controls.Add(this.lblArtistas);
            this.tabPage3.Controls.Add(this.pictureBox1);
            this.tabPage3.Controls.Add(this.dgvCanciones);
            this.tabPage3.Controls.Add(this.dgvAlbumes);
            this.tabPage3.Controls.Add(this.dgvArtistas);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1010, 696);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ABC-API";
            // 
            // lblIntrucciones
            // 
            this.lblIntrucciones.AutoSize = true;
            this.lblIntrucciones.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntrucciones.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblIntrucciones.Location = new System.Drawing.Point(290, 19);
            this.lblIntrucciones.Name = "lblIntrucciones";
            this.lblIntrucciones.Size = new System.Drawing.Size(112, 19);
            this.lblIntrucciones.TabIndex = 47;
            this.lblIntrucciones.Text = "Instrucciónes:";
            // 
            // lblAlbumes
            // 
            this.lblAlbumes.AutoSize = true;
            this.lblAlbumes.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlbumes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblAlbumes.Location = new System.Drawing.Point(433, 428);
            this.lblAlbumes.Name = "lblAlbumes";
            this.lblAlbumes.Size = new System.Drawing.Size(82, 19);
            this.lblAlbumes.TabIndex = 46;
            this.lblAlbumes.Text = "Albumes:";
            // 
            // lblCanciones
            // 
            this.lblCanciones.AutoSize = true;
            this.lblCanciones.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCanciones.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCanciones.Location = new System.Drawing.Point(476, 136);
            this.lblCanciones.Name = "lblCanciones";
            this.lblCanciones.Size = new System.Drawing.Size(96, 19);
            this.lblCanciones.TabIndex = 45;
            this.lblCanciones.Text = "Canciones:";
            // 
            // btnGuardarAlbumes
            // 
            this.btnGuardarAlbumes.Location = new System.Drawing.Point(918, 428);
            this.btnGuardarAlbumes.Name = "btnGuardarAlbumes";
            this.btnGuardarAlbumes.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarAlbumes.TabIndex = 44;
            this.btnGuardarAlbumes.Text = "Guardar";
            this.btnGuardarAlbumes.UseVisualStyleBackColor = true;
            this.btnGuardarAlbumes.Click += new System.EventHandler(this.btnGuardarAlbumes_Click);
            // 
            // btnGuardarCanciones
            // 
            this.btnGuardarCanciones.Location = new System.Drawing.Point(918, 132);
            this.btnGuardarCanciones.Name = "btnGuardarCanciones";
            this.btnGuardarCanciones.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarCanciones.TabIndex = 43;
            this.btnGuardarCanciones.Text = "Guardar";
            this.btnGuardarCanciones.UseVisualStyleBackColor = true;
            this.btnGuardarCanciones.Click += new System.EventHandler(this.btnGuardarCanciones_Click);
            // 
            // btnGuardarArtistas
            // 
            this.btnGuardarArtistas.Location = new System.Drawing.Point(307, 428);
            this.btnGuardarArtistas.Name = "btnGuardarArtistas";
            this.btnGuardarArtistas.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarArtistas.TabIndex = 42;
            this.btnGuardarArtistas.Text = "Guardar";
            this.btnGuardarArtistas.UseVisualStyleBackColor = true;
            this.btnGuardarArtistas.Click += new System.EventHandler(this.btnGuardarArtistas_Click);
            // 
            // btnGuardarCategoria
            // 
            this.btnGuardarCategoria.Location = new System.Drawing.Point(241, 136);
            this.btnGuardarCategoria.Name = "btnGuardarCategoria";
            this.btnGuardarCategoria.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarCategoria.TabIndex = 41;
            this.btnGuardarCategoria.Text = "Guardar";
            this.btnGuardarCategoria.UseVisualStyleBackColor = true;
            this.btnGuardarCategoria.Click += new System.EventHandler(this.btnGuardarCategoria_Click);
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategoria.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCategoria.Location = new System.Drawing.Point(21, 136);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(91, 19);
            this.lblCategoria.TabIndex = 40;
            this.lblCategoria.Text = "Categoria:";
            // 
            // dgvCategoria
            // 
            this.dgvCategoria.AllowUserToOrderColumns = true;
            this.dgvCategoria.AutoGenerateColumns = false;
            this.dgvCategoria.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvCategoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategoria.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noCategoriaDataGridViewTextBoxColumn,
            this.nombredeCategoriaDataGridViewTextBoxColumn});
            this.dgvCategoria.DataSource = this.categoriaBindingSource;
            this.dgvCategoria.Location = new System.Drawing.Point(23, 170);
            this.dgvCategoria.Name = "dgvCategoria";
            this.dgvCategoria.Size = new System.Drawing.Size(293, 164);
            this.dgvCategoria.TabIndex = 39;
            // 
            // noCategoriaDataGridViewTextBoxColumn
            // 
            this.noCategoriaDataGridViewTextBoxColumn.DataPropertyName = "NoCategoria";
            this.noCategoriaDataGridViewTextBoxColumn.HeaderText = "NoCategoria";
            this.noCategoriaDataGridViewTextBoxColumn.Name = "noCategoriaDataGridViewTextBoxColumn";
            // 
            // nombredeCategoriaDataGridViewTextBoxColumn
            // 
            this.nombredeCategoriaDataGridViewTextBoxColumn.DataPropertyName = "NombredeCategoria";
            this.nombredeCategoriaDataGridViewTextBoxColumn.HeaderText = "NombredeCategoria";
            this.nombredeCategoriaDataGridViewTextBoxColumn.Name = "nombredeCategoriaDataGridViewTextBoxColumn";
            this.nombredeCategoriaDataGridViewTextBoxColumn.Width = 150;
            // 
            // categoriaBindingSource
            // 
            this.categoriaBindingSource.DataMember = "Categoria";
            this.categoriaBindingSource.DataSource = this.mUSICABDataSet;
            // 
            // mUSICABDataSet
            // 
            this.mUSICABDataSet.DataSetName = "MUSICABDataSet";
            this.mUSICABDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblArtistas
            // 
            this.lblArtistas.AutoSize = true;
            this.lblArtistas.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArtistas.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblArtistas.Location = new System.Drawing.Point(16, 432);
            this.lblArtistas.Name = "lblArtistas";
            this.lblArtistas.Size = new System.Drawing.Size(65, 19);
            this.lblArtistas.TabIndex = 33;
            this.lblArtistas.Text = "Artistas:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(173, 111);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // dgvCanciones
            // 
            this.dgvCanciones.AllowUserToOrderColumns = true;
            this.dgvCanciones.AllowUserToResizeRows = false;
            this.dgvCanciones.AutoGenerateColumns = false;
            this.dgvCanciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCanciones.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvCanciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCanciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noCancionDataGridViewTextBoxColumn,
            this.nombredeCancionDataGridViewTextBoxColumn,
            this.aniodeEstrenoDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1});
            this.dgvCanciones.DataSource = this.cancionesBindingSource;
            this.dgvCanciones.Location = new System.Drawing.Point(480, 170);
            this.dgvCanciones.Name = "dgvCanciones";
            this.dgvCanciones.RowHeadersVisible = false;
            this.dgvCanciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCanciones.Size = new System.Drawing.Size(513, 213);
            this.dgvCanciones.TabIndex = 30;
            // 
            // noCancionDataGridViewTextBoxColumn
            // 
            this.noCancionDataGridViewTextBoxColumn.DataPropertyName = "NoCancion";
            this.noCancionDataGridViewTextBoxColumn.HeaderText = "NoCancion";
            this.noCancionDataGridViewTextBoxColumn.Name = "noCancionDataGridViewTextBoxColumn";
            // 
            // nombredeCancionDataGridViewTextBoxColumn
            // 
            this.nombredeCancionDataGridViewTextBoxColumn.DataPropertyName = "NombredeCancion";
            this.nombredeCancionDataGridViewTextBoxColumn.HeaderText = "NombredeCancion";
            this.nombredeCancionDataGridViewTextBoxColumn.Name = "nombredeCancionDataGridViewTextBoxColumn";
            // 
            // aniodeEstrenoDataGridViewTextBoxColumn
            // 
            this.aniodeEstrenoDataGridViewTextBoxColumn.DataPropertyName = "AniodeEstreno";
            this.aniodeEstrenoDataGridViewTextBoxColumn.HeaderText = "AniodeEstreno";
            this.aniodeEstrenoDataGridViewTextBoxColumn.Name = "aniodeEstrenoDataGridViewTextBoxColumn";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "FKNombredeArtista";
            this.dataGridViewTextBoxColumn1.DataSource = this.artistasBindingSource;
            this.dataGridViewTextBoxColumn1.DisplayMember = "NombredeArtista";
            this.dataGridViewTextBoxColumn1.HeaderText = "FKNombredeArtista";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn1.ValueMember = "NoArtista";
            // 
            // artistasBindingSource
            // 
            this.artistasBindingSource.DataMember = "Artistas";
            this.artistasBindingSource.DataSource = this.mUSICABDataSet;
            // 
            // cancionesBindingSource
            // 
            this.cancionesBindingSource.DataMember = "Canciones";
            this.cancionesBindingSource.DataSource = this.mUSICABDataSet;
            // 
            // dgvAlbumes
            // 
            this.dgvAlbumes.AllowUserToOrderColumns = true;
            this.dgvAlbumes.AllowUserToResizeRows = false;
            this.dgvAlbumes.AutoGenerateColumns = false;
            this.dgvAlbumes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAlbumes.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvAlbumes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlbumes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noAlbumDataGridViewTextBoxColumn,
            this.nombredelAlbumDataGridViewTextBoxColumn,
            this.contenidodeCancionesDataGridViewTextBoxColumn,
            this.aniodeEstrenoDataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgvAlbumes.DataSource = this.albumesBindingSource;
            this.dgvAlbumes.Location = new System.Drawing.Point(437, 460);
            this.dgvAlbumes.Name = "dgvAlbumes";
            this.dgvAlbumes.RowHeadersVisible = false;
            this.dgvAlbumes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAlbumes.Size = new System.Drawing.Size(556, 213);
            this.dgvAlbumes.TabIndex = 26;
            // 
            // noAlbumDataGridViewTextBoxColumn
            // 
            this.noAlbumDataGridViewTextBoxColumn.DataPropertyName = "NoAlbum";
            this.noAlbumDataGridViewTextBoxColumn.HeaderText = "NoAlbum";
            this.noAlbumDataGridViewTextBoxColumn.Name = "noAlbumDataGridViewTextBoxColumn";
            // 
            // nombredelAlbumDataGridViewTextBoxColumn
            // 
            this.nombredelAlbumDataGridViewTextBoxColumn.DataPropertyName = "NombredelAlbum";
            this.nombredelAlbumDataGridViewTextBoxColumn.HeaderText = "NombredelAlbum";
            this.nombredelAlbumDataGridViewTextBoxColumn.Name = "nombredelAlbumDataGridViewTextBoxColumn";
            // 
            // contenidodeCancionesDataGridViewTextBoxColumn
            // 
            this.contenidodeCancionesDataGridViewTextBoxColumn.DataPropertyName = "ContenidodeCanciones";
            this.contenidodeCancionesDataGridViewTextBoxColumn.HeaderText = "ContenidodeCanciones";
            this.contenidodeCancionesDataGridViewTextBoxColumn.Name = "contenidodeCancionesDataGridViewTextBoxColumn";
            // 
            // aniodeEstrenoDataGridViewTextBoxColumn1
            // 
            this.aniodeEstrenoDataGridViewTextBoxColumn1.DataPropertyName = "AniodeEstreno";
            this.aniodeEstrenoDataGridViewTextBoxColumn1.HeaderText = "AniodeEstreno";
            this.aniodeEstrenoDataGridViewTextBoxColumn1.Name = "aniodeEstrenoDataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FKNombredeArtista";
            this.dataGridViewTextBoxColumn2.DataSource = this.artistasBindingSource;
            this.dataGridViewTextBoxColumn2.DisplayMember = "NombredeArtista";
            this.dataGridViewTextBoxColumn2.HeaderText = "FKNombredeArtista";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.ValueMember = "NoArtista";
            // 
            // albumesBindingSource
            // 
            this.albumesBindingSource.DataMember = "Albumes";
            this.albumesBindingSource.DataSource = this.mUSICABDataSet;
            // 
            // dgvArtistas
            // 
            this.dgvArtistas.AllowUserToOrderColumns = true;
            this.dgvArtistas.AllowUserToResizeRows = false;
            this.dgvArtistas.AutoGenerateColumns = false;
            this.dgvArtistas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvArtistas.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvArtistas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArtistas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noArtistaDataGridViewTextBoxColumn,
            this.nombredeArtistaDataGridViewTextBoxColumn,
            this.fKTipodeCategoriaDataGridViewTextBoxColumn});
            this.dgvArtistas.DataSource = this.artistasBindingSource;
            this.dgvArtistas.Location = new System.Drawing.Point(16, 462);
            this.dgvArtistas.Name = "dgvArtistas";
            this.dgvArtistas.RowHeadersVisible = false;
            this.dgvArtistas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArtistas.Size = new System.Drawing.Size(376, 213);
            this.dgvArtistas.TabIndex = 25;
            // 
            // noArtistaDataGridViewTextBoxColumn
            // 
            this.noArtistaDataGridViewTextBoxColumn.DataPropertyName = "NoArtista";
            this.noArtistaDataGridViewTextBoxColumn.FillWeight = 76.14214F;
            this.noArtistaDataGridViewTextBoxColumn.HeaderText = "NoArtista";
            this.noArtistaDataGridViewTextBoxColumn.Name = "noArtistaDataGridViewTextBoxColumn";
            // 
            // nombredeArtistaDataGridViewTextBoxColumn
            // 
            this.nombredeArtistaDataGridViewTextBoxColumn.DataPropertyName = "NombredeArtista";
            this.nombredeArtistaDataGridViewTextBoxColumn.FillWeight = 111.9289F;
            this.nombredeArtistaDataGridViewTextBoxColumn.HeaderText = "NombredeArtista";
            this.nombredeArtistaDataGridViewTextBoxColumn.Name = "nombredeArtistaDataGridViewTextBoxColumn";
            // 
            // fKTipodeCategoriaDataGridViewTextBoxColumn
            // 
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.DataPropertyName = "FKTipodeCategoria";
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.DataSource = this.categoriaBindingSource;
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.DisplayMember = "NombredeCategoria";
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.FillWeight = 111.9289F;
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.HeaderText = "FKTipodeCategoria";
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.Name = "fKTipodeCategoriaDataGridViewTextBoxColumn";
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fKTipodeCategoriaDataGridViewTextBoxColumn.ValueMember = "NoCategoria";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.webControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(988, 676);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Web-API";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // webControl2
            // 
            this.webControl2.BackColor = System.Drawing.SystemColors.Highlight;
            this.webControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webControl2.Location = new System.Drawing.Point(3, 3);
            this.webControl2.Name = "webControl2";
            this.webControl2.Size = new System.Drawing.Size(982, 670);
            this.webControl2.TabIndex = 0;
            this.webControl2.Load += new System.EventHandler(this.webControl2_Load);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.localControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(988, 676);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Local-API";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // localControl1
            // 
            this.localControl1.BackColor = System.Drawing.SystemColors.Desktop;
            this.localControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.localControl1.Location = new System.Drawing.Point(3, 3);
            this.localControl1.Name = "localControl1";
            this.localControl1.Size = new System.Drawing.Size(982, 670);
            this.localControl1.TabIndex = 0;
            // 
            // ABC
            // 
            this.ABC.Controls.Add(this.tabPage1);
            this.ABC.Controls.Add(this.tabPage2);
            this.ABC.Controls.Add(this.tabPage3);
            this.ABC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ABC.Location = new System.Drawing.Point(0, 0);
            this.ABC.Name = "ABC";
            this.ABC.SelectedIndex = 0;
            this.ABC.Size = new System.Drawing.Size(1018, 722);
            this.ABC.TabIndex = 0;
            // 
            // categoriaTableAdapter
            // 
            this.categoriaTableAdapter.ClearBeforeFill = true;
            // 
            // artistasTableAdapter
            // 
            this.artistasTableAdapter.ClearBeforeFill = true;
            // 
            // cancionesTableAdapter
            // 
            this.cancionesTableAdapter.ClearBeforeFill = true;
            // 
            // albumesTableAdapter
            // 
            this.albumesTableAdapter.ClearBeforeFill = true;
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.listBox1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 17;
            this.listBox1.Items.AddRange(new object[] {
            "1.- Para agregar solo escriba los campos de la tabla. ",
            "2.- Para editar clic en el campo que desea editar.",
            "3.- Para eliminar selecciona un campo de la fila y presiona tecla suprimir o dele" +
                "te de su teclado.",
            "NOTA!",
            "CUALQUIER CAMBIO REALIZADO, SE DEBERA DARLE CLIC AL BOTON GUARDAR."});
            this.listBox1.Location = new System.Drawing.Point(417, 8);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(585, 89);
            this.listBox1.TabIndex = 48;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // ExampleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 722);
            this.Controls.Add(this.ABC);
            this.Name = "ExampleForm";
            this.Text = "SpoitfyAPI .NET Example";
            this.Load += new System.EventHandler(this.ExampleForm_Load);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mUSICABDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCanciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.artistasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancionesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlbumes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArtistas)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ABC.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl ABC;
        private System.Windows.Forms.TabPage tabPage1;
        private LocalControl localControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private WebControl webControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvCategoria;
        private System.Windows.Forms.Label lblArtistas;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgvCanciones;
        private System.Windows.Forms.DataGridView dgvAlbumes;
        private System.Windows.Forms.DataGridView dgvArtistas;
        private System.Windows.Forms.Label lblCategoria;
        private MUSICABDataSet mUSICABDataSet;
        private System.Windows.Forms.BindingSource categoriaBindingSource;
        private MUSICABDataSetTableAdapters.CategoriaTableAdapter categoriaTableAdapter;
        private System.Windows.Forms.Button btnGuardarCategoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn noCategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombredeCategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource artistasBindingSource;
        private MUSICABDataSetTableAdapters.ArtistasTableAdapter artistasTableAdapter;
        private System.Windows.Forms.Button btnGuardarArtistas;
        private System.Windows.Forms.Label lblCanciones;
        private System.Windows.Forms.Button btnGuardarAlbumes;
        private System.Windows.Forms.Button btnGuardarCanciones;
        private System.Windows.Forms.Label lblAlbumes;
        private System.Windows.Forms.BindingSource cancionesBindingSource;
        private MUSICABDataSetTableAdapters.CancionesTableAdapter cancionesTableAdapter;
        private System.Windows.Forms.BindingSource albumesBindingSource;
        private MUSICABDataSetTableAdapters.AlbumesTableAdapter albumesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn noCancionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombredeCancionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aniodeEstrenoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn noAlbumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombredelAlbumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contenidodeCancionesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aniodeEstrenoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn noArtistaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombredeArtistaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn fKTipodeCategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label lblIntrucciones;
        private System.Windows.Forms.ListBox listBox1;
    }
}

