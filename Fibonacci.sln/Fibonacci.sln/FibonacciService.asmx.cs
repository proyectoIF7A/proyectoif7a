﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Fibonacci.sln
{
    /// <summary>
    /// Summary description for FibonacciService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FibonacciService : System.Web.Services.WebService
    {

        [WebMethod]
        public long fibonacciNumber(long number)
        {
            return fibonacciRecursivo(number);
        }

        static void Main(string[] args)
        { Console.WriteLine("Introduzca el numero a calcular: ");
            int n = int.Parse(Console.In.ReadLine());

        //Fibonacci Recursivo
        DateTime tiempo1 = DateTime.Now;//Se obtiene la medida de tiempos ANTES del fibonacci recursivo 
        Console.WriteLine("Fibonacci Recursivo para n = " + n + ": " + fibonacciRecursivo(n));//Se ejecuta el fibonacci recursivo 
            DateTime tiempo2 = DateTime.Now;//Se vuelve a tomar el tiempo despues de la ejecucion 
        Console.WriteLine("Tiempo del Fibonacci recursivo: " + new TimeSpan(tiempo2.Ticks - tiempo1.Ticks) + "\n");//Tiempo total despues de la ejecucion del fibonacci recursivo 

    }
        public static long fibonacciRecursivo(long n)
        {
            if (n == 0) return 0;
            else if (n == 1) return 1;
            else return fibonacciRecursivo(n - 1) + fibonacciRecursivo(n - 2);//Llamada recursiva con el elemento n-1 + n-2 
        }

    }
}
