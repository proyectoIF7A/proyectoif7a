﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ProyectoWebServiceAsmx
{
    /// <summary>
    /// Summary description for BDWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BDWS : System.Web.Services.WebService
    {

        /* [WebMethod]
         public string HelloWorld()
         {
             return "Hello World";
         }*/
      /*  [WebMethod]
        public DataSet GetData()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = "Data Source=LocalHost; Initial Calalog=EscuelaBD; Integrated Security=True";
            SqlDataAdapter da = new SqlDataAdapter("Select Nombre,Apellidos from Alumnos where NoControl='142141'", con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }*/

        [WebMethod]
        public string Guardar_Alumno(int NoControl, string Nombre, string Apellidos, string Carrera, int NoProfesor)
      {
       Alumno alumno = new Alumno();
            alumno.nocontrol = NoControl;
            alumno.nombre = Nombre;
            alumno.apellidos = Apellidos;
            alumno.carrera = Carrera;
            alumno.noprofesor = NoProfesor;
           
            AccesoDatos accesoDatos = new AccesoDatos();
            return "Alumno guardados";

        }

        [WebMethod]
        public Alumno Consultar_Alumno(int NoControl)
        {
            Alumno alumno = new Alumno();
           
            AccesoDatos accesoDatos = new AccesoDatos();
            alumno = accesoDatos.ConsultaAlumno(NoControl);

            return alumno;
        }

        [WebMethod]
        public string Actualizar_Alumno(int NoControl, string Nombre, string Apellidos, string Carrera, int NoProfesor)
        {
            Alumno alumno = new Alumno();
            alumno.nocontrol = NoControl;
            alumno.nombre = Nombre;
            alumno.apellidos = Apellidos;
            alumno.carrera = Carrera;
            alumno.noprofesor = NoProfesor;
            AccesoDatos accesoDatos = new AccesoDatos();
            return "Alumno actualizar";
        }


        [WebMethod]
        public string Eliminar_Alumno(int NoControl, string Nombre, string Apellidos, string Carrera, int NoProfesor)
        {
            Alumno alumno = new Alumno();
            alumno.nocontrol = NoControl;
            alumno.nombre = Nombre;
            alumno.apellidos = Apellidos;
            alumno.carrera = Carrera;
            alumno.noprofesor = NoProfesor;
            AccesoDatos accesoDatos = new AccesoDatos();
            return "Alumno eliminado";
        }
    }
    }
//}
