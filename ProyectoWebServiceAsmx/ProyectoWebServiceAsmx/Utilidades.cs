﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using ProyectoWebServiceAsmx;

namespace ProyectoWebServiceAsmx
{
    public static class Utilidades
    {

        public static string Obtenercadenadeconexion()
        {
            return ConfigurationManager.AppSettings["EscuelaBD"].ToString();
        }
    }
}