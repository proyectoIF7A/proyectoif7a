﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoWebServiceAsmx
{
    [Serializable]
    public class Alumno
    {
        #region Atributos
        int NoControl;
        string Nombre;
        string Apellidos;
        string Carrera;
        int NoProfesor;
        #endregion
        #region propiedades
        public int nocontrol { get { return NoControl; } set { NoControl = value; } }
        public string nombre { get { return Nombre; } set { Nombre = value; } }
        public string apellidos { get { return Apellidos; } set { Apellidos = value; } }
        public string carrera { get { return Carrera; } set { Carrera = value; } }
        public int noprofesor { get { return NoProfesor; } set { NoProfesor = value; } }
        #endregion
    }
}
    