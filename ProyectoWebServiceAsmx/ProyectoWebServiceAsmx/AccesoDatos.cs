﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProyectoWebServiceAsmx;
using System.Data.SqlClient;
using System.Data;

namespace ProyectoWebServiceAsmx
{
    public class AccesoDatos

    {
        //  Guardar
        public void GuardarAlumno(Alumno alumno)
        {
            try
            {
                string CadenaConexion;
                CadenaConexion = Utilidades.Obtenercadenadeconexion();

                SqlConnection Conexion = new SqlConnection(CadenaConexion);
                Conexion.Open();

                SqlCommand Command = new SqlCommand("SP_Guadar_Alumnno", Conexion);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("NoControl", alumno.nocontrol);
                Command.Parameters.AddWithValue("Nombre", alumno.nombre);
                Command.Parameters.AddWithValue("Apellidos", alumno.apellidos);
                Command.Parameters.AddWithValue("Carrera", alumno.carrera);
                Command.ExecuteNonQuery();
                Conexion.Close();
            }
            catch (Exception)
            {
                throw;

            }
        }

        // Actualizar
                    public void ActualizarAlumno(Alumno alumno)
        {
            try
            {
                string CadenaConexion;
                CadenaConexion = Utilidades.Obtenercadenadeconexion();

                SqlConnection Conexion = new SqlConnection(CadenaConexion);
                Conexion.Open();

                SqlCommand Command = new SqlCommand("SP_ActualizarAlumno", Conexion);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("NoControl", alumno.nocontrol);
                Command.Parameters.AddWithValue("Nombre", alumno.nombre);
                Command.Parameters.AddWithValue("Apellidos", alumno.apellidos);
                Command.Parameters.AddWithValue("Carrera", alumno.carrera);
                Command.ExecuteNonQuery();
                Conexion.Close();
            }
            catch (Exception)
            {
                throw;

            }
        }

        internal Alumno ConsultaAlumno(int noControl)
        {
            throw new NotImplementedException();
        }

        //Consulta
        public void ConsultaAlumno(Alumno alumno)
        {
            try
            {
                string CadenaConexion;
                CadenaConexion = Utilidades.Obtenercadenadeconexion();

                SqlConnection Conexion = new SqlConnection(CadenaConexion);
                Conexion.Open();

                SqlCommand Command = new SqlCommand("SP_Consulta_Alumno", Conexion);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("NoControl", alumno.nocontrol);
                Command.Parameters.AddWithValue("Nombre", alumno.nombre);
                Command.Parameters.AddWithValue("Apellidos", alumno.apellidos);
                Command.Parameters.AddWithValue("Carrera", alumno.carrera);
                Command.ExecuteNonQuery();
                Conexion.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Eliminar
                    public void EliminarAlumno(string NoControl)
        {
            try
            {
                string CadenaConexion;
                CadenaConexion = Utilidades.Obtenercadenadeconexion();

                SqlConnection Conexion = new SqlConnection(CadenaConexion);
                Conexion.Open();

                SqlCommand Command = new SqlCommand("SP_Consulta_Alumno", Conexion);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("NoControl", NoControl);
                Command.ExecuteNonQuery();
                Conexion.Close();
            }
            catch (Exception)
            {
                throw;

            }

        }
    }
}