﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;//utilizar este espacio de nombres para sockets
using System.Net;//para el direccionamiento IP
using System.IO;//para streaming io
using System.Threading;//para ejecutar hilos



namespace ProyectoSocket
{
    //los delegados se utilizan para manipular controles como cuadro de texto y cuadro de lista....etc de los hilos, son una interfaz.
    //necesitamos que este delegado cambie el contenido del cuadro de texto(mensajes recibidos)
    delegate void UpdateTextBox(string msg);
    
    public partial class Cliente : Form
    {
        private TcpClient Client;//variable necesaria para escuchar las conexiones
        private BinaryReader MessageReader;//variable para leer mensajes
        private BinaryWriter MessageWriter;//variable para escribir mensajes
        private NetworkStream DataStream;//variable para mantener el servidor y el cliente en un flujo y sincronizado
        private Thread ClientThread;//variable que se asigna a un hilo que escucha las conexiones entrantes e impide que el PC bloquee
        byte[] bytes = new byte[1024];
        Socket senderSock;
        public Cliente()
        {
            InitializeComponent();
            button2.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // intenta si la dirección IP está bien escrita
            try
            {
                IPAddress.Parse(textBox3.Text);//
                ClientThread = new Thread(new ThreadStart(PerformConnection));
                ClientThread.Start();
            }
            catch (Exception)
            {
                MessageBox.Show("Dirección IP incorrecta");//señale el error en un cuadro de mensaje
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {       //comprobar si el su es una conexión primero
                try
                {
                    if (Client.Connected)
                    {
                        // MessageWriter.Write(textBox2.Text);//enviar mensaje vía stream
                        ///MessageWriter.Write(textBox4.Text);
                        double A=0, B=0, C;
                        A = double.Parse(textBox2.Text);
                        B = double.Parse(textBox4.Text);
                        C = A - B;                                       
                        MessageWriter.Write("Resultado es: " + textBox2.Text + "-" + textBox4.Text + "=" + C.ToString() + "\r\n");
                        textBox2.Clear();//cuadro de texto claro después del envío
                        textBox4.Clear();
                        // int A, B;
                        //  string resultado;
                        //string value = Convert.ToString(0,99);
                        /* string theMessageToSend = textBox2.Text + " , " + textBox3.Text;
                        /* A = int.Parse(textBox2.Text);
                         B = int.Parse(textBox4.Text);
                         resultado = ((A) - (B));*/
                        // resultado = ((A)-(B));
                        //theMessageToSend = (A - B); 
                        //   MessageBox.Show("A" + "-" + "B"+ "=" + resultado.ToString);        
                        /* byte[] msg = Encoding.Unicode.GetBytes(theMessageToSend);
                         int bytesSend = senderSock.Send(msg);
                         button2.Enabled = true;
                         button3.Enabled = true;
                         DataFromServer();*/
                    }
                    //if (Cliente)

                }
                catch (Exception)
                {
                    MessageBox.Show("No hay cliente conectado");//signal the error in a messagebox
                }
            }
        }


        private void DataFromServer()
        {
            {
                try
                {
                    int bytesRec = senderSock.Receive(bytes);
                    String theMessageToReceive = Encoding.Unicode.GetString(bytes, 0, bytesRec);
                    while (senderSock.Available > 0)
                    {
                        bytesRec = senderSock.Receive(bytes);
                        theMessageToReceive += Encoding.Unicode.GetString(bytes, 0, bytesRec);
                    }
                    textBox1.Text = theMessageToReceive;
                }
                catch (Exception exc) { MessageBox.Show(exc.ToString()); }
            }

        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            //if enter key was pressed
            if (e.KeyCode == Keys.Enter)
            {
                //check if the their is a connection first
                try
                {
                    if (Client.Connected)
                    {
                        MessageWriter.Write(textBox2.Text);//send message via stream
                        textBox2.Clear();//clear text box after sending
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("No hay cliente conectado");//signal the error in a messagebox
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);//exit and close all threads and release all recources
        }

        //network functions used
        private void PerformConnection()
        {
           // int numVal = -1;
            //bool repeat = true;
            //try Connecting on the give ip address
            try
            {
                

                Client = new TcpClient();//assign new tcp client object
                ChangeTextBoxContent("Conectando......");
                Client.Connect(IPAddress.Parse(textBox3.Text), 80);//connect to given ip on port 80 allways
                DataStream = Client.GetStream();
                MessageReader = new BinaryReader(DataStream);
                MessageWriter = new BinaryWriter(DataStream);
                ChangeTextBoxContent("Conectado");
                HandleConnection();
                // numVal = Convert.ToInt32
               // textBox1.Text = Convert.ToInt64("abc");

               // MessageBox.Show(textBox1.Text -  textBox2.Text)
                MessageWriter.Close();
                MessageReader.Close();
                DataStream.Close();
                Client.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("No se puede conectar, dirección IP incorrecta");//signal the error in a message box
            }
        }
        private void HandleConnection()
        {
            string message;
            //loop until infinity
            do
            {
                //try reading from the data stream if anything went wrong with the connection break
                try
                {
                    message = MessageReader.ReadString();//read message
                    ChangeTextBoxContent(message);//call the function that manipulates text box from a thread and change the contents.
                }
                catch (Exception)
                {
                    ChangeTextBoxContent("Conexión perdida");
                    break;//get out of the while loop
                }
            } while (true);
        }
        private void ChangeTextBoxContent(string tx)
        {
            if (textBox1.InvokeRequired)//if the messages text box needs a delegate invoking
            {
                Invoke(new UpdateTextBox(ChangeTextBoxContent), new object[] { tx });
            }
            else
            {
                //if no invoking required then change
                textBox1.Text += tx + "\r\n";//concatinate the original with the given message and a new line
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Cliente_Load(object sender, EventArgs e)
        {
            Servidor S = new Servidor();
            S.Show();
        }

       /* private void button3_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    senderSock.Shutdown(SocketShutdown.Both);
                    senderSock.Close();
                    button3.Enabled = false;
                }
                catch (Exception exc) {
                    MessageBox.Show(exc.ToString()); }
            }
        }*/
    }
}
