﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;//para streaming io
using System.Linq;
using System.Net;//para el direccionamiento
using System.Net.Sockets;//utilizar este espacio de nombres para sockets
using System.Text;
using System.Threading;//para ejecutar hilos
using System.Windows.Forms;

namespace ProyectoSocket
{
    // delegados se utilizan para manipular controles como cuadro de texto y cuadro de lista .... etc de los hilos, son una interfaz.
    // necesitamos que este delegado cambie el contenido del cuadro de texto (mensajes recibidos)
    // delegate void UpdateTextBox (string msg);
    public partial class Servidor : Form
    {
        private TcpListener ConnectionListener;// variable necesaria para escuchar las conexiones
        private BinaryReader MessageReader;//variable para leer mensajes
        private BinaryWriter MessageWriter;//variable para escribir mensajes
        private Socket ClientConnection;//variable para mantener la conexión del cliente
        private NetworkStream DataStream;//variable para mantener el servidor y el cliente en un flujo y sincronizado
        private Thread ListeningThread;//variable que se asigna a un hilo que escucha las conexiones entrantes e impide que el PC bloquee
        public Servidor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try if the ip address is written well 
            try
            {
                IPAddress.Parse(textBox3.Text);//
                ListeningThread = new Thread(new ThreadStart(ListenForConnections));//assign thread variable with the blocking function
                ListeningThread.Start();//start the thread that will wait for connections
            }
            catch (Exception)
            {
                MessageBox.Show("Dirección IP incorrecta");//signal the error in a message box
            }
        }

        private void button2_Click(object sender, EventArgs e)
        { 

            //check if the their is a client first
            try
            {
                if (ClientConnection.Connected)
                {

                    MessageWriter.Write(textBox1.Text);//send message via stream
                    textBox1.Clear();//clear text box after sending
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No hay cliente conectado");//signal the error in a messagebox
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)//check if enter key was pressed
            {
                //check if the their is a client first
                try
                {
                    if (ClientConnection.Connected)
                    {

                        //double num1, num2, ope;                      
                        // num1 = textBox1.Text;
                        // num2 = textBox1.Text;
                        // ope = num1 - num2;
                        //ope = textBox1.Text + "-" + textBox2;
                        MessageWriter.Write(textBox1.Text);
                            //+ "," + (textBox22.Text));//send message via stream
                        textBox1.Clear();//clear text box after sending
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("No hay cliente conectado");//signal the error in a messagebox
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);//exit and close all threads and release all recources
        }

        //functions used for networks
        private void ListenForConnections()
        {
            //try listening with the given ip address
            try
            {
                ConnectionListener = new TcpListener(IPAddress.Parse(textBox3.Text), 80);//listen to given ip on port 80 allways
                ConnectionListener.Start();//start listening;
                ChangeTextBoxContent("Servidor Activo");
                ClientConnection = ConnectionListener.AcceptSocket();//wait untill client connects (blocking function) if connected return a socket 
                DataStream = new NetworkStream(ClientConnection);//initialize a stream 
                MessageReader = new BinaryReader(DataStream);//use reader within the stream
                MessageWriter = new BinaryWriter(DataStream);//use writer within the stream
                ChangeTextBoxContent("Conexión recibida");
                HandleConnection();//handle the reading and writing 
                //here the client disconnected or something went wrong with the connection
                MessageReader.Close();//close the reader;
                MessageWriter.Close();//close the writer;
                DataStream.Close();//close the stream;
                ClientConnection.Close();//close thre connection
            }
            catch (Exception)
            {
                MessageBox.Show("No se puede conectar, dirección IP incorrecta");//signal the error in a message box
            }
        }
        private void HandleConnection()
        {
            string message;
            //loop until infinity
            do
            {
                //try reading from the data stream if anything went wrong with the connection break
                try
                {
                    message = MessageReader.ReadString();//read message
                    ChangeTextBoxContent(message);//call the function that manipulates text box from a thread and change the contents.
                }
                catch (Exception)
                {
                    ChangeTextBoxContent("Conexión perdida");
                    break;//get out of the while loop
                }
            } while (true);
        }
        private void ChangeTextBoxContent(string tx)
        {
            if (textBox1.InvokeRequired)//if the messages text box needs a delegate invoking
            {
                Invoke(new UpdateTextBox(ChangeTextBoxContent), new object[] { tx });
            }
            else
            {
                //if no invoking required then change
                textBox1.Text += tx + "\r\n";//concatinate the original with the given message and a new line
            }
        }

        //class Resta
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox22_TextChanged(object sender, EventArgs e)
        {
            /* Cliente C = new Cliente();
             textBox1.Text = textBox22.Text;
             C.Show();*/
           // MessageWriter.Write(textBox22.Text);
        }
    }
}
