CREATE DATABASE EscuelaBD
USE EscuelaBD

CREATE TABLE Alumnos (
NoControl int primary key not null,
Nombre varchar(30),
Apellidos varchar(50),
Carrera varchar(50),
NoProfesor int,
ProfesoresID int foreign key (NoProfesor) references Profesores(IDProfesor)
);

CREATE TABLE Profesores (
IDProfesor int primary key not null,
NombreProfesor varchar(40),
GradodeEstudios varchar(50),
Especialidad varchar(50),
--AlumnoNo int,
--AlumnosID int foreign key (AlumnoNo) references Alumnos(NoControl)
);

CREATE TABLE Materias (
IDMateria int primary key not null,
NombreMateria varchar(40),
NoProfe int,
ProfesoresIDD int foreign key (NoProfe) references Profesores(IDProfesor)
);

CREATE TABLE Clase (
IDClase int primary key not null,
NoAlumno int, NoProfesor int, NoMaterias int,
AlumnosID int foreign key (NoAlumno) references Alumnos(NoControl),
ProfesoresID int foreign key (NoProfesor) references Profesores(IDProfesor),
MateriasID int foreign key (NoMaterias) references Materias(IDMateria)
);
 


--Consultas
---------------------INSERT VALUES---------------------------
--Insert into Alumnos values ('142140','Juan','Perez Lopez','Ing. Informatica')
Insert into Alumnos(NoControl, Nombre, Apellidos, Carrera, NoProfesor) values ('142141','Juan','Castro Lopez', 'Ing. Informatica', '100')
Insert into Alumnos(NoControl, Nombre, Apellidos, Carrera, NoProfesor) values ('142142','Maria','Hernandez Diaz', 'Ing. Informatica', '100')
Insert into Alumnos(NoControl, Nombre, Apellidos, Carrera, NoProfesor) values ('142143','Pedro','Aviles Lopez', 'Ing. Informatica', '100')
Insert into Alumnos(NoControl, Nombre, Apellidos, Carrera, NoProfesor) values ('142144','Alex','Rubio Soto', 'Ing. Informatica', '100')

Select * from Alumnos

Insert into Profesores values('100','Francisco Gomez', 'Maestria en ciencias de la computacion','Inteligencia Artificial')
Insert into Profesores values('200','Carlos Hurtado', 'Doctorado en ciencias de la computacion','Seguridad Informatica')
Insert into Profesores values('300','Francisco Gomez', 'Maestria en ciencias de la computacion','Interconectividad de Redes')
Insert into Profesores values('400','Luis Gomez', 'Maestria en ciencias de la computacion','Sistemas de Informacion')

Select * from Profesores

Insert into Materias(IDMateria,NombreMateria,NoProfe) values ('111','Taller de Base de Datos','100')
Insert into Materias(IDMateria,NombreMateria,NoProfe) values ('222','Calculo Integral','200')
Insert into Materias(IDMateria,NombreMateria,NoProfe) values ('333','Redes de computadoras','300')
Insert into Materias(IDMateria,NombreMateria,NoProfe) values ('444','Fundamentos de Base de datos','400')

Select * from Materias

Insert into Clase(IDClase,NoAlumno,NoProfesor,NoMaterias) values('1','142141','100','111')
Insert into Clase(IDClase,NoAlumno,NoProfesor,NoMaterias) values('2','142141','200','222')
Insert into Clase(IDClase,NoAlumno,NoProfesor,NoMaterias) values('3','142141','300','333')
Insert into Clase(IDClase,NoAlumno,NoProfesor,NoMaterias) values('4','142141','400','444')

Select*from Clase

---------------------SELECT
Select Nombre,Apellidos from Alumnos where NoControl='142141'