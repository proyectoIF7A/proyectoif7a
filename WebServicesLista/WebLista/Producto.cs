﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLista
{
    public class Producto
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public int CantidadArticulos { get; set; }
        public decimal TotalPrecio { get; set; }
    }
}