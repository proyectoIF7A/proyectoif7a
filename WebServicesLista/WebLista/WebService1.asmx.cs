﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebLista
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

    /* public class  Producto
        {
            public int ID { get; set; }
            public string Nombre { get; set; }
            public decimal Precio { get; set; }
            public int CantidadArticulos { get; set; }
            public decimal TotalPrecio { get; set; }
        }
        */

        //----------------Agregar Producto-----------------//
        [WebMethod(EnableSession = true)]

        public List<Producto>Agregar_Productos(int id, string nombre, decimal precio, int cantidadArticulos)
        {
            List<Producto> Lista;

        if (Session["Usuario"] == null)
            {  Lista = new List<Producto>(); }

            else {
                  Lista = (List<Producto>)
                  Session["Usuario"];}

            Producto Producto = new Producto
            {
                ID = id, Nombre = nombre, Precio = precio, CantidadArticulos = cantidadArticulos,
                TotalPrecio = cantidadArticulos * precio,
            };
            Lista.Add(Producto);
            Session["Usuario"] = Lista;
            return Lista;
        }

        //----------------Ver Producto-----------------//
        [WebMethod(EnableSession = true)]

        public List<Producto> Ver_Productos()
        {
            if (Session["Usuario"] == null)
            {
                List<Producto> Lista = new List<Producto>();
                return Lista;
            }
            else
            {
                return (List<Producto>)Session["Usuario"];
            }
        }


        //-----------------Eliminar Productos---------------//
        [WebMethod(EnableSession = true)]
        public string Eliminar_Productos(int id)
        {
            List<Producto> Lista;
            if (Session["Usuario"] == null)
            {
                Lista = new List<Producto>();
            }
            else
            {
                Lista = (List<Producto>)Session["Usuario"];
            }
            foreach (var Productos in Lista)
            {
                if (Productos.ID == id)
                {
                    Lista.Remove(Productos);
                    Session["Usuario"] = Lista;
                    break;
                }
            }
            return "Producto eliminado = " + id.ToString();
        }


        //----------------Producto Mayor-----------------//
        [WebMethod(EnableSession = true)]
        public List<Producto> Producto_Mayor(decimal precio)
        {
            if (Session["Usuario"] == null)
            {
                List<Producto> Lista = new List<Producto>();
                return Lista;
            }

            else
            {
                List<Producto> Lista = (
                    from Productos 
                    in Ver_Productos()
                    where Productos.Precio > precio
                    select Productos).ToList();

                return Lista;
            }
            //Lista.FindAll(Producto.ID, Producto.Nombre, Producto.Precio)


        }

        //-------------Producto Menor------------------------//
        [WebMethod(EnableSession = true)]
        public List<Producto> Producto_Menor(decimal precio)
        {
            if (Session["Usuario"] == null)
            {
                List<Producto> Lista = new List<Producto>();

                return Lista;
            }

            else
            {
                List<Producto> Lista = (from Productos in Ver_Productos()
                                        where Productos.Precio < precio
                                        select Productos).ToList();
                return Lista;
            }
            //Lista.FindAll(Producto.ID, Producto.Nombre, Producto.Precio)


        }
        //--------------Producto Repetidos---------------//
        [WebMethod(EnableSession = true)]

        public List<Producto> Producto_Repetidos()
        {
            if (Session["Usuario"] == null)
            {
                List<Producto> Lista = new List<Producto>();//"No has agregado ningun producto a tu Lista"
                Producto productos = new Producto
                { Nombre = "No datos repetido",};
                Lista.Add(productos);
                return Lista;
            }
            else
            {
                List<Producto> Lista = (from Producto in Ver_Productos()
                                        where Producto.CantidadArticulos > 1
                                        select Producto).ToList();
                return Lista;
            }
        }

        //------------- Producto Total--------------------//
        [WebMethod(EnableSession = true)]
        public string Precio_Total()
        {
            if (Session["Usuario"] == null)
            {
                return "No se agrego el producto";
            }
            else
            {
                List<Producto> Lista = (from Producto 
                                        in Ver_Productos()
                                        select Producto).ToList();
                var PrecioTotal = Lista.Sum(x => x.TotalPrecio);
                return "Total de la compra=" + PrecioTotal.ToString();
            }
        }
    }
}